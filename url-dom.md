# Lendo URLs com DOM

Escrevi essa função há um tempo atrás e
[até publiquei](http://tassoevan.me/posts/parsing-urls-with-dom). Deve ser útil
para alguém.

A estrutura das URLs é complicada e **você não deve confiar nos dados enviados
pelo usuário**, pois nada impede que eles sejam fornecidos por uma agente
ilegítimo. Ler URLs de qualquer jeito é abrir mão da segurança do usuário do seu
sistema.

Eis abaixo um método fácil de ler URLs utilizando Javascript e o Document Object
Model (DOM): criar um elemento de âncora, atribuir a URL para a propriedade
`href` e ler as demais propriedades, como `scheme`, `host`, path etc.

```js
function parseURL(url)
{
	var a = document.createElement('a');
	a.href = url;
	return {
	    'href': a.href
	  , 'scheme': a.protocol
	  , 'host': a.host
	  , 'port': a.port
	  , 'path': a.pathname
	  , 'query': a.search.charAt(0) == '?' ? a.search.substring(1) : null
	  , 'hash': a.hash.charAt(0) == '#' ? a.hash.substring(1) : null
	};
}
```